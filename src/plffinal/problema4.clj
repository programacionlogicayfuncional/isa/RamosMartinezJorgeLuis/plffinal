(ns plffinal.problema4
  (:require [plffinal.utilidades :as util]))

(defn mismo-punto-final?
  [xs ys]
  (= (last (util/recorrer xs)) (last (util/recorrer ys))))

(mismo-punto-final? "" [])
(mismo-punto-final? "^^^" "<^^^>")
(mismo-punto-final? [\< \< \< \>] (list \< \<))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

(mismo-punto-final? "" "<")
(mismo-punto-final? [\> \>] "<>")
(mismo-punto-final? [\> \> \>] [\> \> \> \>])
(mismo-punto-final? (list) (list \^))


