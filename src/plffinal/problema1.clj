(ns plffinal.problema1
  (:require [plffinal.utilidades :as util]))

(defn regresa-al-punto-de-origen?
  [xs]
  (= {:x 0 :y 0} (last (util/recorrer xs))))

(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))

(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])


