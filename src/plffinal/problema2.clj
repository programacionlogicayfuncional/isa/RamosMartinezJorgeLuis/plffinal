(ns plffinal.problema2
  (:require [plffinal.problema1 :as pr1]))

(defn regresan-al-punto-de-origen?
  [& args]
  (empty? (filter false? (map pr1/regresa-al-punto-de-origen? args))))


(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? [])

(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))

(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])
