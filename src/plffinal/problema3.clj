(ns plffinal.problema3
  (:require [plffinal.problema1 :as pr1]))

(defn regreso-al-punto-de-origen
  [xs]
  (if (pr1/regresa-al-punto-de-origen? xs) 
    '() 
    (map (fn [x] (cond
                 (= x \<) \>
                 (= x \>) \<
                 (= x \v) \^
                 (= x \^) \v
                 :else '())) (reverse xs))))


(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))

(regreso-al-punto-de-origen ">>>")
(regreso-al-punto-de-origen [\< \v \v \v \> \>])
(regreso-al-punto-de-origen (list \> \> \> \< \<))
