(ns plffinal.utilidades)

(defn recorrer
  [xs]
  (letfn [(g [c punto]
           (cond
             (= c \<) (update punto :x dec)
             (= c \>) (update punto :x inc)
             (= c \^) (update punto :y inc)
             (= c \v) (update punto :y dec)
             :else punto))
          (f [ys punto]
             (if (empty? ys)
               [punto]
               (flatten (conj [punto] (f (rest ys) (g (first ys) punto))))))]
    (f xs {:x 0 :y 0})))

