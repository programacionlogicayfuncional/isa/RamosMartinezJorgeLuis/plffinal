(ns plffinal.problema5
  (:require [plffinal.utilidades :as util]))

(defn coincidencias
  [xs ys]
   (let [coordenadas (into (util/recorrer xs) (util/recorrer ys))]
     (- (count coordenadas) (count (set coordenadas)))))

(coincidencias "" [])
(coincidencias (list \< \<) [\> \>])

(coincidencias [\^ \> \> \> \^] ">^^<")
(coincidencias "<<vv>>^>>" "vv<^")
(coincidencias ">>>>>" [\> \> \> \> \>])
(coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))