(ns plffinal.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plffinal.problema1 :as pr1]
            [plffinal.problema2 :as pr2]
            [plffinal.problema3 :as pr3]
            [plffinal.problema4 :as pr4]
            [plffinal.problema5 :as pr5]))

(deftest regresa-al-punto-de-origen?-test
  (testing "Problema 1 Test 1"
    (is (= true (pr1/regresa-al-punto-de-origen? ""))))
  (testing "Problema 1 Test 2"
    (is (= true (pr1/regresa-al-punto-de-origen? []))))
  (testing "Problema 1 Test 3"
    (is (= true (pr1/regresa-al-punto-de-origen? (list)))))
  (testing "Problema 1 Test 4"
    (is (= true (pr1/regresa-al-punto-de-origen? "><"))))
  (testing "Problema 1 Test 5"
    (is (= true (pr1/regresa-al-punto-de-origen? (list \> \<)))))
  (testing "Problema 1 Test 6"
    (is (= true (pr1/regresa-al-punto-de-origen? "v^"))))
  (testing "Problema 1 Test 7"
    (is (= true (pr1/regresa-al-punto-de-origen? [\v \^]))))
  (testing "Problema 1 Test 8"
    (is (= true (pr1/regresa-al-punto-de-origen? "^>v<"))))
  (testing "Problema 1 Test 9"
    (is (= true (pr1/regresa-al-punto-de-origen? (list \^ \> \v \<)))))
  (testing "Problema 1 Test 10"
    (is (= true (pr1/regresa-al-punto-de-origen? "<<vv>>^^"))))
  (testing "Problema 1 Test 11"
    (is (= true (pr1/regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^]))))
  (testing "Problema 1 Test 12"
    (is (= false (pr1/regresa-al-punto-de-origen? ">"))))
  (testing "Problema 1 Test 13"
    (is (= false (pr1/regresa-al-punto-de-origen? (list \>)))))
  (testing "Problema 1 Test 14"
    (is (= false (pr1/regresa-al-punto-de-origen? "<^"))))
  (testing "Problema 1 Test 15"
    (is (= false (pr1/regresa-al-punto-de-origen? [\< \^]))))
  (testing "Problema 1 Test 16"
    (is (= false (pr1/regresa-al-punto-de-origen? ">>><<"))))
  (testing "Problema 1 Test 17"
    (is (= false (pr1/regresa-al-punto-de-origen? (list \> \> \> \< \<)))))
  (testing "Problema 1 Test 18"
    (is (= false (pr1/regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))

(deftest regresan-al-punto-de-origen?-test
  (testing "Problema 2 Test 1"
    (is (= true (pr2/regresan-al-punto-de-origen?))))
  (testing "Problema 2 Test 2"
    (is (= true (pr2/regresan-al-punto-de-origen? []))))
  (testing "Problema 2 Test 3"
    (is (= true (pr2/regresan-al-punto-de-origen? ""))))
  (testing "Problema 2 Test 4"
    (is (= true (pr2/regresan-al-punto-de-origen? [] "" (list)))))
  (testing "Problema 2 Test 5"
    (is (= true (pr2/regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) ""))))
  (testing "Problema 2 Test 6"
    (is (= true (pr2/regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v)))))
  (testing "Problema 2 Test 7"
    (is (= false (pr2/regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<]))))
  (testing "Problema 2 Test 8"
    (is (= false (pr2/regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>"))))
  (testing "Problema 2 Test 8"
    (is (= false (pr2/regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))))

(deftest regreso-al-punto-de-origen-test
  (testing "Problema 3 Test 1"
    (is (= '() (pr3/regreso-al-punto-de-origen ""))))
  (testing "Problema 3 Test 2"
    (is (= '() (pr3/regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v)))))
  (testing "Problema 3 Test 3"
    (is (= '(\< \< \<) (pr3/regreso-al-punto-de-origen ">>>"))))
  (testing "Problema 3 Test 4"
    (is (= '(\< \< \^ \^ \^ \>) (pr3/regreso-al-punto-de-origen [\< \v \v \v \> \>])))))

(deftest mismo-punto-final?
  (testing "Problema 4 Test 1"
    (is (= true (pr4/mismo-punto-final? "" []))))
  (testing "Problema 4 Test 2"
    (is (= true (pr4/mismo-punto-final? "^^^" "<^^^>"))))
  (testing "Problema 4 Test 3"
    (is (= true (pr4/mismo-punto-final? [\< \< \< \>] (list \< \<)))))
  (testing "Problema 4 Test 4"
    (is (= true (pr4/mismo-punto-final? (list \< \v \>) (list \> \v \<)))))
  (testing "Problema 4 Test 5"
    (is (= false (pr4/mismo-punto-final? "" "<"))))
  (testing "Problema 4 Test 6"
    (is (= false (pr4/mismo-punto-final? [\> \>] "<>"))))
  (testing "Problema 4 Test 7"
    (is (= false (pr4/mismo-punto-final? [\> \> \>] [\> \> \> \>]))))
  (testing "Problema 4 Test 8"
    (is (= false (pr4/mismo-punto-final? (list) (list \^))))))

(deftest coincidencias
  (testing "Problema 5 Test 1"
    (is (= 1 (pr5/coincidencias "" []))))
  (testing "Problema 5 Test 2"
    (is (= 1 (pr5/coincidencias (list \< \<) [\> \>]))))
  (testing "Problema 5 Test 3"
    (is (= 2 (pr5/coincidencias [\^ \> \> \> \^] ">^^<"))))
  (testing "Problema 5 Test 4"
    (is (= 4 (pr5/coincidencias "<<vv>>^>>" "vv<^"))))
  (testing "Problema 5 Test 5"
    (is (= 6 (pr5/coincidencias ">>>>>" [\> \> \> \> \>]))))
  (testing "Problema 5 Test 6"
    (is (= 6 (pr5/coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))))))